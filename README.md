## How to start

Install dependencies:

```bash
npm install
```

Run fake API:

```bash
npm run mock:api
```

Run the page, on different terminal:

```bash
npm run start
```