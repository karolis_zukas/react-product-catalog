const constants = {
    API_URL: 'http://localhost:4000/data',
    CARDS_PER_PAGE: 20,
    NEXT_PAGES_LIMIT: 3
}

export default constants;