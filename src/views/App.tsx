import * as React from 'react';
import { Landing } from '../components/Landing/Landing';
import { useFetch } from 'react-hooks-async';
import ProductList from '../views/ProductList/ProductList';
import { Product, SortingTypes } from '../types/types';
import Loader from '../components/Loader/Loader';
import constants from '../constants';
import '../styles/main.scss';

export const App = () => {
    const [ currentPageNumber, setCurrentPageNumber ] = React.useState(0);
    const [ currentSortingState, setCurrentSortingState ] = React.useState(0);
    const { pending, error, result } = useFetch(constants.API_URL);
    const topOfListRef = React.createRef<HTMLDivElement>();
    let pageSet: Product[];

    if (pending) return <Loader />;
    if (error) return <>Ups, something went wrong</>;


    const getPageSlice = (initialSet: Product[], pageNumber: number): Product[] => {
        return initialSet.slice(pageNumber * constants.CARDS_PER_PAGE, (pageNumber + 1) * constants.CARDS_PER_PAGE);
    }

    if (result) {
        pageSet = getPageSlice(result, currentPageNumber);
    }

    const onSortingChange = (sortOperator: SortingTypes ) => {
        const sortedByPrice = result.sort((a: Product, b: Product) => {
            if (sortOperator === SortingTypes.DSC) {
                if (Number(b.actual_price) > Number(a.actual_price)) return 1;
                if (Number(a.actual_price) > Number(b.actual_price)) return -1;
            } else {
                if (Number(a.actual_price) > Number(b.actual_price)) return 1;
                if (Number(b.actual_price) > Number(a.actual_price)) return -1;
            }
        });
        pageSet = getPageSlice(sortedByPrice, currentPageNumber);
        setCurrentSortingState(sortOperator);
    }

    const handlePageClick = (pageNumber: number) => {
        setCurrentPageNumber(pageNumber - 1);
        pageSet = getPageSlice(result, currentPageNumber);
        window.scrollTo(0, topOfListRef.current.offsetTop);
    }

    const handlePreviousClick = () => {
        if (currentPageNumber > 0) {
            setCurrentPageNumber(currentPageNumber - 1);
            pageSet = getPageSlice(result, currentPageNumber);
            window.scrollTo(0, topOfListRef.current.offsetTop);
        }
    }

    const handleNextClick = () => {
        if (currentPageNumber <= Math.ceil(result.length / constants.CARDS_PER_PAGE) - 2) {
            setCurrentPageNumber(currentPageNumber + 1);
            pageSet = getPageSlice(result, currentPageNumber);
            window.scrollTo(0, topOfListRef.current.offsetTop);
        }
    }

    return (
        <>
            <Landing 
                title="Hey Boozt"
                subtitle="This is a pagination project by Karolis Žukas"
                activeSorting={currentSortingState}
                onSortingChange={(type) => onSortingChange(type)}
                />
            <div ref={topOfListRef}></div>
            <ProductList
                products={ pageSet }
                pagesCount={ Math.ceil(result.length / constants.CARDS_PER_PAGE) }
                currentPage={ currentPageNumber }
                handlePageClick={ handlePageClick }
                handlePreviousClick={ handlePreviousClick }
                handleNextClick={ handleNextClick }
            />
        </>
        )
};