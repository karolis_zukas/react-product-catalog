import * as React from 'react';
import Card from '../../components/Card/Card';
import constants from '../../constants';
import { Product } from '../../types/types';
import Pagination from '../../components/Pagination/Pagination';
import './ProductList.scss';

type ProductListProps = {
    products: Product[];
    pagesCount: number;
    currentPage: number;
    handlePageClick: Function;
    handlePreviousClick: Function;
    handleNextClick: Function;
};

const ProductList: React.FunctionComponent<ProductListProps> = (props) => {
    const [ activePageItems, setActivePageItems ] = React.useState(props.products);

    React.useEffect(() => {
        const newItemsPage = [...props.products];
        setActivePageItems(newItemsPage);
    }, [props]);

    return (
        <>
            <div className="product-list">
                {activePageItems.map((element: Product) => <Card product={element} key={element.id} />)}
            </div>
            <Pagination {...props} />
        </>
        )
};

export default ProductList;
