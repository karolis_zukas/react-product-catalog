export type Product = {
    id: string;
    product_name: string;
    brand_name: string;
    actual_price: number;
    base_price: number;
    filename: string;
    image_loaded: boolean;
    image_loading: boolean;
};

export enum SortingTypes {
    ASC = 1,
    DSC = 2
};