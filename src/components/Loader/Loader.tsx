import * as React from 'react';
import './Loader.scss';

const Loader: React.FunctionComponent<{}> = () => {

    return (
        <div className="loading-animation-container">
            <span className="dot"></span>
            <div className="dots">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div>
    )
};

export default Loader;