import * as React from 'react';
import './Pagination.scss';
import constants from '../../constants';
import PaginationBox from '../Pagination-box/Pagination-box';

type PaginationProps = {
    pagesCount: number;
    currentPage: number;
    handlePageClick: Function;
    handlePreviousClick: Function;
    handleNextClick: Function;
}

const Pagination: React.FunctionComponent<PaginationProps> = (props) => {
    const nextPage = props.currentPage + 2;
    const { NEXT_PAGES_LIMIT } = constants;

    const getNextPageNumbers = () => {
        let pagesList = [];

        for (let i = nextPage; i < nextPage + NEXT_PAGES_LIMIT; i++) {
            if (i <= props.pagesCount) {
                pagesList.push(i);
            };
        }

        return pagesList.map(pageNumber => (
            <PaginationBox key={pageNumber} onClick={() => props.handlePageClick(pageNumber)} value={pageNumber} />
        ));
    }

    const getPreviousPageNumbers = () => {
        let pagesList = [];

        for (let i = props.currentPage - NEXT_PAGES_LIMIT; i < props.currentPage; i++) {
            if (i >= 0) {
                pagesList.push(i + 1);
            };
        }

        return pagesList.map(pageNumber => (
            <PaginationBox key={pageNumber} onClick={() => props.handlePageClick(pageNumber)} value={pageNumber} />
        ));
    }

    const getPreviousButton = () => {
        if (props.currentPage > 0) {
            return (
                <PaginationBox onClick={() => props.handlePreviousClick()} value="<" />
            )};
    }

    const getNextButton = () => {
        if (nextPage <= props.pagesCount) {
            return (
                <PaginationBox onClick={() => props.handleNextClick()} value=">" />
            )};
    }


    return (
        <div className="pagination">
            { getPreviousButton() }
            { getPreviousPageNumbers() }
            <PaginationBox value={props.currentPage + 1}  isCurrentPage={true} />
            { getNextPageNumbers() }
            { getNextButton() }
        </div>
        )
};

export default Pagination;