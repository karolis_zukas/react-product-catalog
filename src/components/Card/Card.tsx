import * as React from 'react';
import './Card.scss';
import { Product } from '../../types/types';

type CardProps = {
    product: Product;
}

const Card: React.FunctionComponent<CardProps> = ({ product }) => {
    const { product_name, brand_name, actual_price, base_price, filename } = product;
    const [imageLoaded, setImageLoaded] = React.useState(false);

    const displayProductInfoOverlay = () => {
        if (imageLoaded) {
            return (
                <div className="card-overlay">
                <div className="card-items"></div>
                <div className="card-items-title">
                    <p>{brand_name} {product_name}</p>
                    <hr />
                </div>

                <div className="card-items-price">
                    <p className="card-items-price--old">{base_price}</p>
                    <p className="card-items-price--new">{actual_price}</p>
                </div>

                <div className="card-items-logo">
                    <span>ADD TO CART</span>
                </div>
            </div>
            );
        } else {
            return null;
        }
    }

    return (
        <div className="card-container">
            <img
                alt={product_name}
                src={filename}
                onLoad={() => setImageLoaded(true)}
            />
            {displayProductInfoOverlay()}
        </div>
        )

};

export default Card;