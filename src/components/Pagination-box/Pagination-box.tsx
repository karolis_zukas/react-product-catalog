import * as React from 'react';
import './Pagination-box.scss';

type PaginationBoxProps = {
    isCurrentPage?: boolean;
    value: string | number;
    onClick?: Function;
}

const PaginationBox: React.FunctionComponent<PaginationBoxProps> = ({ value, isCurrentPage, onClick} ) => {

    return (
        <div className="pagination-control">
            <div onClick={() => isCurrentPage ? null : onClick ? onClick() : null} className={ isCurrentPage ? "current-page-container" : "container"} >
                { value }
            </div>
        </div>
    )

};

export default PaginationBox;