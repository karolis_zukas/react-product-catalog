import * as React from 'react';
import './Landing.scss';
import { SortingTypes } from '../../types/types';

type LandingProps = {
    title: string;
    subtitle: string;
    activeSorting: SortingTypes;
    onSortingChange: Function;
}

export const Landing: React.FunctionComponent<LandingProps> = (props) => {

    return (
        <>
            <div className="landing landing--full-screen">
                <h1 className="title">{ props.title }</h1>
                <h2 className="subtitle">{ props.subtitle }</h2>

                <h2 className="landing-title--small">About</h2>
                <p>
                    So, first things first. You would definitely want to have, a backend, which would handle the pagination.
                    I don't think handling 2000 products in one call is a good idea. Nevertheless, as I was running out of time,
                    I decided to serve everthing up as is. What can be done make life easier with this app?
                </p>
                <ul>
                    <li>Mobile UX is not the best with hover effects. Cards on mobile needs to be displayed with price, and name</li>
                    <li>Redux for sure. Transforming Array of products, to entities would be great improvement moving on. 
                        (faster lookup, of individual products).
                    </li>
                    <li>Queue for image loading. Preloading images on next page, before it gets into view.</li>
                    <li>Unit tests. Lots of unit tests.</li>
                </ul>
            </div>
            <div className="sorting-controls">
                <h3 className="sorting-controls-title">Sort by: </h3>
                <p
                    className={ props.activeSorting === SortingTypes.ASC ? "sorting-controls-button--active sorting-controls-button" : "sorting-controls-button"}
                    onClick={() => props.onSortingChange(SortingTypes.ASC)}>
                        Price ascending
                </p>
                <p
                    className={ props.activeSorting === SortingTypes.DSC ? "sorting-controls-button--active sorting-controls-button" : "sorting-controls-button"}
                    onClick={() => props.onSortingChange(SortingTypes.DSC)}>
                        Price descending
                </p>
            </div>
        </>
        )
};